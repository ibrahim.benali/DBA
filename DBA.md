#### 1) Créer TABLESPACE tbl_gc_com répartie en deux fichier :
    -  C:\oraclexe\oradata\xe\fd01tbl_gest.dbf de taille 10 Mo extensible de 2M
    -  C:\oraclexe\oradata\xe\fd02tbl_gest.dbf de taille fix de 10Mo

`CREATE TABLESPACE "tbl_gc_com" DATAFILE "C:\oraclexe\oradata\xe\fd01tbl_gest.dbf" SIZE  10M
    AUTOEXTEND 2M, "C:\oraclexe\oradata\xe\fd02tbl_gest.dbf" SIZE 10M;
`

#### 2) Ajouter au tablespace tbl_gest_com un fichier nommé fd03tbl_gest.dbf de taille 15M
`ALTER TABLESPACE "tbl_gc_com" ADD DATAFILE "C:\oraclexe\oradata\xe\fd03tbl_gest.dbf" SIZE 15M;` 

#### 3) Créer un TABLESPACE temporaire temp_gest_com de taille 15M contenant un fichier :
    -  C:\oraclexe\oradata\XE\ftemp.dbf
`CREATE TEMPORARY TABLESPACE "temp_gest_com" TEMPFILE "C:\oraclexe\oradata\XE\ftemp.dbf" SIZE 15M;`

#### 4) Modifier la base de données afin de rendre le tablespace temp_gest_com comme tablespace temporaire par défault de la base.
`ALTER DATABASE default TEMPORARY TABLESPACE "temp_gest_com"`

#### 5) Créer un profil g_c_profile ayant les limites suivantes : 
    - Le temp de connexion est limité à 45 minutes,
    - Le temp d'inactivitéest limitéà 5 minutes,
    - La durée de vie du mot de passe est 180 minutes,
    - Le nombre de connexion simultanées est 2,
    - Le  compte sera verrouillé pendant un jour après 3 tentatives de connexions.
>[FAILED_LOGIN_ATTEMPTSmax_value 3],
>[PASSWORD_LOCK_TIMEmax_value 1]// en Jr

`CREATE PROFILE "g_c_profile" LIMIT` <br>
    `CONNECT_TIME 45` <br>
    `IDLE_TIME 5` <br>
    `PASSWORD_LIFE_TIME 180/1440` <br>
    `SESSIONS_PER_USER 2` <br>
    `FAILED_LOGIN_ATTEMPTS 3` <br>
    `PASSWORD_LOCK_TIME 1;` <br>

#### 6) Créer un rôle g_c_role ayant : 
    - le privilège de connexion à la base de données avec la possibilité de le transmettre, 
    - les privilèges de lecture et d’insertion sur la table EMPLOYEES de l'utilisateur HR,
    - le rôle RESOURCE.

` CREATE ROLE "g_c_role";` <br>
` GRANT CONNECT TO g_c_role WITH ADMIN OPTION;` <br>
` GRANT SELECT, INSERT ON RH.EMPLOYEES TO g_c_role;` <br>
` GRANT RESOURCE TO g_c_role;` <br>

#### 7) Créer deux utilisateurs : 
##### A) g_c_manager avec 
    - un mot de passe g_c_manager, 
    - tbl_gest_com comme tablespace par défaut,
    - le rôle g_c _role et le profil g_c _profile,
    - le compte doit être verrouillé. 

`CREATE USER g_c_manager IDENTIFIED BY g_c_manager` <br>
`DEFAULT TABLESPACE tbl_gest_com` <br>
`PROFILE g_c_profile` <br>
`ACCOUNT LOCK;` <br>
`GRANT g_c_role TO g_c_manager;` <br>

##### B) g_c_user avec 
    - un mot de passe g_c_user expiré, 
    - tbl_gest_com comme tablespace par défaut,
    - le rôle g_c _role et le profil g_c _profile.

`CREATE USER g_c_user IDENTIFIED BY g_c_user` <br>
`DEFAULT TABLESPACE tbl_gest_com` <br>
`PROFILE g_c_profile` <br>
`PASSWORD EXPIRE;` <br>
`GRANT g_c_role TO g_c_user;` <br>

#### 8) Déverrouiller le compte g_c_manager
    `ALTER USER g_c_manager ACCOUNT UNLOCK;`
#### 9) La société cherche à charger la base à partir d'un fichier de données. Le fichier est à base de séparateur  « ; ». L’opération de chargement suivant le tableau de mapping suivant:
   
| Nom | colonne Signification | Type |
|---  |-----------------------|------|
| CLIENT_ID |	Code client | Entier(4) |
| CLIENT_NAME | Nom client | Chaine(30) |
| ADRESSE_CL | Adresse client | Chaine(30) |
| CREATE_DATE | Date de création | Date |

>NB : Le chargement se fera avec le compte g_c_manager.
##### A/ Créer la table selon la structure suivante : 

| Nom de la table | Client | 
|----             |--------:|
|Attributs 	| CLIENT_ID  <br> CLIENT_NAME  <br> ADRESSE_CL <br> CREATE_DATE |
   
`CREATE TABLE client ( CLIENT_ID number(4), 
CLIENT_NAME VARCHAR(30), 
ADRESSE_CL VARCHAR(30), 
CREATE_DATE DATE);`

##### B/ Ecrire le contenu du fichier de contrôle qui permet d’assurer le chargement des données à partir du fichier

|                    |             |
|--------------------|------------:|
| Fichier de données |	C:\datafile.dat |
| Fichier bad	| C:\badfile.bad |
| Fichier discarde	| C:\discfile.dsc |
| Condition de chargement |	Si 01:02 = '22' |

>controlfile.ctl

    LOAD DATA INFILE 'C:\datafile.dat' BADFILE 'C:\badfile.bad' DISCARDFILE 'C:\discfile.dsc'
    INSERT INTO CLIENT CLIENT WHERE(01:02) = '22' 
    FIELDS TERMINATED BY ';'
    (CLIENT_ID, CLIENT_NAME, ADRESSE_CL, CREATE_DATE);

##### C/ Ecrire la requête qui permet de lancer le chargement.

|                    |             |
|--------------------|------------:|
| Fichier log | C:\logfile.log |

`SQLLD g_c_manager/g_c_manager CONTROL='C:\controlfile.ctl' LOG='C:\logfile.log';`

#### 10) Créer un Directory User_Oracle sous le chemin ‘C:\oraclexe\app\oracle’.

`CREATE OR REPLACE DIRECTORY User_oracle AS 'C:\oraclexe\app\oracle'`


#### 11) Attribuer aux utilisateurs g_c_manager et g_c_user les privilèges suivants :
    - Import et export de données,
    - Lecture et écriture dans le répertoire User_Oracle

`GRANT EXP_FULL_DATABASE, IMP_FULL_DATABASE TO g_c_manager, g_c_user;`
`GRANT WRITE ON DIRECTORY User_oracle to g_c_manager, g_c_user;`

#### 12) Donner les commandes nécessaires afin d’exporter (Métadata et Data) la table Client  de l’utilisateur g_c_manager et de l’importer sous le schéma de g_c_user (utiliser le directory User_oracle).

`EXPDP g_c_manager/g_c_manger DIRECTORY=User_oracle DUMPFILE=exp_client.dmp LOGFILE=exp_client.log;`
`IMPDP g_c_user/g_c_user DIRECTORY=User_oracle DUMPFILE=exp_client.dmp LOGFILE=imp_client.log;`

